const path = require('path');
const sqlite3 = require("sqlite3").verbose();
const db_name = path.join(__dirname, "data", "quings.db");
const db = new sqlite3.Database(db_name, err => {
  if (err) {
    return console.error(err.message);
  }
});

const sql_create = 'CREATE TABLE IF NOT EXISTS Quings (Name VARCHAR(255) NOT NULL, Place VARCHAR(255), Link VARCHAR(255))';

db.run(sql_create, err => {
  if (err) {
    return console.error(err.message);
  }
});

const jsdom = require("jsdom");
const dom = new jsdom.JSDOM(`<!DOCTYPE html>`);
var $ = require("jquery")(dom.window);
var async = require("async");


function makeSPARQLQuery( endpointUrl, sparqlQuery, doneCallback ) {
	var settings = {
		headers: { Accept: 'application/sparql-results+json' },
		data: { query: sparqlQuery }
	};
	return $.ajax( endpointUrl, settings ).then( doneCallback );
}

var endpointUrl = 'https://query.wikidata.org/sparql',
	sparqlQuery = "#recherche de l'ensemble des Drag Queens\n" +
        "SELECT DISTINCT ?item ?itemLabel ?country ?countryLabel\n" +
        "WHERE \n" +
        "{\n" +
        "  {?item wdt:P106 wd:Q337084 } UNION {?item wdt:P106 wd:Q1254525}\n" +
        "  OPTIONAL{?item wdt:P27 ?country.}\n" +
        "  SERVICE wikibase:label { bd:serviceParam wikibase:language \"[AUTO_LANGUAGE],en,fr,es,de\" }\n" +
    "}";

makeSPARQLQuery( endpointUrl, sparqlQuery,
		 function( data ) {
		     rows = data['results']['bindings'];
		     async.forEachOf(rows, function (row, key, callback) {
			 var name = row['itemLabel']['value'];
			 var country = "";
			 if (!row['countryLabel']){
			     country = "Unknown";
			 }else{
			     country = row['countryLabel']['value'];
			 }
			 var link = row['item']['value'];
			 var is_in = "SELECT * FROM Quings WHERE Name = '" + escape(name) + "'";
			 db.all(is_in, [],
				function (err, rows) {
				    if (err){
					return console.error(err.message);
				    }else if (!rows || rows.length == 0){
					var insert = "INSERT INTO Quings(name, place, link) " +
					    "VALUES ('" + escape(name) + "', '" + escape(country) + "', '" + escape(link) + "')";
					console.log(name + " was inserted into the database");
					console.log(insert);
					db.run(insert,
					       function (err){
						   if(err){
						       return console.error(err.message);
						   }
					       }
					      );
				    }
				});
		     }, function (error) {
			 if (error) {
			     console.log(error);
			 }
		     })
		 }
	       );

var express = require('express');
var app = express();
app.listen(3000);
app.set('view engine', 'ejs');
app.use(express.static("public"));

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/map', function(req, res) {
    db.all('SELECT DISTINCT name, place FROM Quings', [],
	   function(err, rows){
	       if (err){
		   return console.error(err.message);
	       }else{
		   res.render('map.ejs', {
		       v_points : rows
		   });
	       }
	   });
});

app.post('/map', function(req, res) {
    res.render('map.ejs', {
	v_points : JSON.parse(req.body.table)
    });
});

app.get('/', function(req, res) {
  var currentURL = req.url;
  var name = "Name";
  var place = "Country";
  var bool = 0;
  if(currentURL.indexOf('?') != -1){
    console.log(currentURL.slice(currentURL.indexOf('?')+1, currentURL.length));
    place = unescape(currentURL.slice(currentURL.indexOf('?')+1, currentURL.length));
    bool = 1;
  }
    res.render('form.ejs', {
	v_name : name,
	v_place : place,
	v_help : "",
  v_bool : bool
    });
});

app.post('/search', function(req, res){
    var name = escape(req.body.name);
    var place = escape(req.body.place);
    if ((name == "Name" || name == "") && (place == "Country" || place == "")) {
	res.render('form.ejs', {
	    v_name : "Name",
	    v_place : "Country",
	    v_help: "<h3>Please enter either a name or a country</h3>",
      v_bool : 0
	});
    }else if (name !== "Name" && name !== "") {
	db.all('SELECT DISTINCT * FROM Quings WHERE name LIKE "%'+ name +'%" ORDER BY name', [],
	       function(err, rows){
		   if (err) {
		       return console.error(err.message);
		       // NO RESULT
		   }else if (!rows || rows.length == 0){
		       res.render('result.ejs', {
			   v_help : "Oh no ! There is no available result :(",
			   v_content : "",
			   v_amount : "",
			   v_table : rows
		       });
		       // SOME RESULT
		   }else{
		       var html = "";
		       for (var i = 0; i < rows.length; i++){
			   html = html + "<article><p>Name : <a href = '" + unescape(rows[i]['Link']) + "'>";
			   html = html + unescape(rows[i]['Name']) + "</a></p>";
			   html = html + "<p>Country : "+ unescape(rows[i]['Place']) + "</p></article>";
		       }
		       var amount = "";
		       if (rows.length == 1){
			   amount = "We found 1 result";
		       }else{
			   amount = "We found " + rows.length + " results";
		       }
		       res.render('result.ejs', {
			   v_help : "Here are your Quings !",
			   v_content : html,
			   v_amount : amount,
			   v_table : rows
		       });
		   }
	       }
	      );
    }else{
	db.all('SELECT DISTINCT * FROM Quings WHERE place LIKE "%'+ place +'%" ORDER BY name', [],
	       function(err,rows){
		   if (err) {
		       return console.error(err.message);
		       // NO RESULT
		   }else if (!rows || rows.length == 0){
		       res.render('result.ejs', {
			   v_help : "Oh no ! There is no available result :(",
			   v_content : "",
			   v_amount : "",
			   v_table : rows
		       });
		       // SOME RESULT
		   }else{
		       var html = "";
		       for (var i = 0; i < rows.length; i++){
			   html = html + "<article><p>Name : <a href = '" + unescape(rows[i]['Link']) + "'>";
			   html = html + unescape(rows[i]['Name']) + "</a></p>";
			   html = html + "<p>Country : "+ unescape(rows[i]['Place']) + "</p></article>";
		       }
		       var amount = "";
		       if (rows.length == 1){
			   amount = "We found 1 result";
		       }else{
			   amount = "We found " + rows.length + " results";
		       }
		       res.render('result.ejs', {
			   v_help : "Here are your Quings !",
			   v_content : html,
			   v_amount : amount,
			   v_table : rows
		       });
		   }
	       }
	      );
    }
});

app.get('/browse', function(req, res){
    db.all('SELECT DISTINCT * FROM Quings ORDER BY name', [],
	   function(err,rows){
	       if (err){
		   return console.error(err.message);
	       }else if (!rows || rows.length == 0){
		   res.render('result.ejs', {
		       v_help : "Oh no ! There is no available result :(",
		       v_content : "",
		       v_amount : "",
		       v_table : rows
		   });
	       }else{
		   var html = "";
		   for (var i = 0; i < rows.length; i++){
		       html = html + "<article><p>Name : <a href = '" + unescape(rows[i]['Link']) + "'>";
		       html = html + unescape(rows[i]['Name']) + "</a></p>";
		       html = html + "<p>Country : "+ unescape(rows[i]['Place']) + "</p></article>";
		   }
		   var amount = "";
		   if (rows.length == 1){
		       amount = "We found 1 result";
		   }else{
		       amount = "We found " + rows.length + " results";
		   }
		   res.render('result.ejs', {
		       v_help : "Here are your Quings !",
		       v_content : html,
		       v_amount : amount,
		       v_table : rows
		   });
	       }
	   }
	  );
});

console.log('Server running at http://127.0.0.1:3000/');
