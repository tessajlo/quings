QUINGS

Presentation : A census of Drag artists collected from Wikidata

What to install to run it :
	nodejs
	npm
	sqlite
	sqlite3
	jquery
	jsdom
	express
	requests
	ejs
	mysql
	sparql

How to run :
	npm start

Authors :
	Samuel NALIN
	Tessa LELIEVRE-OSSWALD
